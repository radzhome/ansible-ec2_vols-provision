# ansible-ec2-vols-provision

Used to provision existing amazon EC2 instances for use with Django / posgres / nginx.
New volumes are created for data. It can also help with turning a server from a pet to cattle.


To setup boto, and ssh keys:

1) install boto

    pip install boto

2) in your bash_profile add AWS Keys & source ie.

    export AWS_ACCESS_KEY_ID=''
    export AWS_SECRET_ACCESS_KEY=''

3) run ssh-add on your ssh key used for the instance




To test or run:

NOTE: complete boto & ssh key setup above first!

1) Provision new instance, note the public ip and instance_id & fill in the information in inventory/ec2env i.e.

    [appservers]
    ec2-52-1-189-67.compute-1.amazonaws.com
    ...
    instance_id: i-49f8d9b3

2) SSH into the instance and install the services i.e. mongodb, mysql, postgresql and make sure the data in the 'sym_link'
for each volume has test data to copy.

3) run on local system:

    ansible-playbook -i inventory/ec2env site.yml



To cleanup after test:

Note: When done testing, don't forget to clean up to minimize AWS costs.

Log into your AWS console and perform instance & vol cleanup:

- Remove the instances manually
- Remove the Volumes


References: http://thinkingeek.com/2014/05/10/create-configure-ec2-instances-rails-deployment-ansible/
